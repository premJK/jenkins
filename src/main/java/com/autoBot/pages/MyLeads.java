package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyLeads extends Annotations{
	
//	public CreateLead clickCreateLead()
//	{
//		driver.findElementByLinkText("Create Lead").click();
//		return new CreateLead();
//	}

	public CreateLead clickCreateLead()
	{
		WebElement eleCreatelead = locateElement("link", "Create Lead");
		click(eleCreatelead);
		return new CreateLead();
	}
}
