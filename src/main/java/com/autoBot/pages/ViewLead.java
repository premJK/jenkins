package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class ViewLead extends Annotations{
	
	public ViewLead verifyFirstName(String VerifyFN)
	{
		WebElement eleFname = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleFname, VerifyFN);
		return this;
	}

}
