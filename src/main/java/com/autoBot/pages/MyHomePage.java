package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;


public class MyHomePage extends Annotations{
	
//	public MyLeads clickLeadsTab()
//	{
//		driver.findElementByLinkText("Leads").click();
//		return new MyLeads();
//	}

	public MyLeads clickLeadsTab()
	{
		WebElement eleLeadstab = locateElement("link", "Leads");
		click(eleLeadstab);
		return new MyLeads();
	}
}
