package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLead extends Annotations{
	
	public CreateLead enterCompanyName(String data)
	{
		WebElement eleCName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCName, data);
		return this;
	}

	public CreateLead enterFirstName(String data)
	{
		WebElement eleFName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFName, data);
		return this;
	}
	
	public CreateLead enterLastName(String data)
	{
		WebElement eleLName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLName, data);
		return this;
	}

	public ViewLead clickCreateLeadButton()
	{
		WebElement eleCreatelead = locateElement("name", "submitButton");
		click(eleCreatelead);
		return new ViewLead();
	}
}
